<!DOCTYPE html>

<html>
<head>
	<title>Termékeink</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Kezdőlap</title>
	<link rel="stylesheet" type="text/css" href="store.css">
	<link rel="stylesheet" type="text/css" 
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<script src="store.js" async></script>
</head>
<body>
	<?php include "./header.php" ?>
		
	</div>
        <section class="container content-section">
            <h2 class="section-header">Mindenféle</h2>
            <div class="shop-items">
                <div class="shop-item">
                    <span class="shop-item-title">Gumikacsa</span>
                    <img class="shop-item-image" src="gumikacsa.jpg">
                    <div class="shop-item-details">
                        <span class="shop-item-price">1500Ft</span>
                        <button class="btn btn-primary shop-item-button" type="button">Kosárba vele!</button>
                    </div>
                </div>
                <div class="shop-item">
                    <span class="shop-item-title">Deszka</span>
                    <img class="shop-item-image" src="deszka.jpg">
                    <div class="shop-item-details">
                        <span class="shop-item-price">2000Ft</span>
                        <button class="btn btn-primary shop-item-button"type="button">Kosárba vele!</button>
                    </div>
                </div>
                <div class="shop-item">
                    <span class="shop-item-title">Teniszlabda</span>
                    <img class="shop-item-image" src="teniszlabda.jpg">
                    <div class="shop-item-details">
                        <span class="shop-item-price">500Ft</span>
                        <button class="btn btn-primary shop-item-button" type="button">Kosárba vele!</button>
                    </div>
                </div>
                <div class="shop-item">
                    <span class="shop-item-title">Labda</span>
                    <img class="shop-item-image" src="labda.jpg">
                    <div class="shop-item-details">
                        <span class="shop-item-price">1000Ft</span>
                        <button class="btn btn-primary shop-item-button" type="button">Kosárba vele!</button>
                    </div>
                </div>
            </div>
        </section>
		
        <section class="container content-section">
            <h2 class="section-header">Kosaram</h2>
            <div class="cart-row">
                <span class="cart-item cart-header cart-column">Termék</span>
                <span class="cart-price cart-header cart-column">Ár</span>
                <span class="cart-quantity cart-header cart-column">Mennyiség</span>
            </div>
            <div class="cart-items">
            </div>
            <div class="cart-total">
                <strong class="cart-total-title">Fizetendő</strong>
                <span class="cart-total-price">0 Ft</span>
            </div>
            <button class="btn btn-primary btn-purchase" type="button">Vásárlás</button>
        </section>
	
	<script>
	function myFunction() {
	  var x = document.getElementById("myTopnav");
	  if (x.className === "topnav") {
		x.className += " responsive";
	  } else {
		x.className = "topnav";
	  }
	}
	</script>
</body>
</html>